#!/bin/bash

function usage
{
    echo "$1 [-c] [-b branch] [-d \"driver,driver,..\"]"
}

function notify
{
#	echo $1 | festival --tts &
	if [ "x$DISPLAY" != "x" ]; then
		if which notify-send > /dev/null; then
	    		notify-send -t 600000 "$1"
		fi
	else
		echo $1
	fi
}

if ! which lndir > /dev/null 2>&1 ; then
    echo lndir must be installed
    exit 1
fi

if [ -d /opt/idr/devel/graphics/Mesa/BUILD ]; then
    cd /opt/idr/devel/graphics/Mesa/BUILD
else
    cd ~/devel/graphics/Mesa/BUILD
fi

typeset -i do_clean=0
typeset BRANCH=master
typeset drivers
typeset prefix
typeset -i do_install=0

ARCH=$(uname -m)
if [ -d /opt/xorg-master-$ARCH ]; then
    prefix=/opt/xorg-master-$ARCH
elif [ -d /opt/xorg/xorg-master-$ARCH ]; then
    prefix=/opt/xorg/xorg-master-$ARCH
else
    echo "Installation directory does not exist."
    exit 1
fi

if grep -q Atom /proc/cpuinfo ; then
    drivers="i915"
else
    drivers="i965,swrast"
fi

OPT="-O0 -mno-omit-leaf-frame-pointer"
debug='--enable-debug'
ARCHES=''
gallium="no"
mode="native"

while getopts "cb:d:D:Gim:O:p:r" flag
do
    case $flag in
    c) do_clean=1;;
    b) BRANCH=$OPTARG;;
    d) drivers=$OPTARG;;
    D) base_dir=$OPTARG;;
    G) gallium="yes";;
    i) do_install=1;;
    m) mode=$OPTARG;;
    O) OPT="$OPTARG";;
    p) prefix=$OPTARG;;
    r) OPT='-O3 -momit-leaf-frame-pointer'; debug='';;
    *) usage $0; exit 1;; 
    esac
done

WARN="-Wall -Wextra -Wunsafe-loop-optimizations -Werror=format-security"
if echo "$OPT" | grep -q -e '-O[^01g]'; then
    DBG="-ggdb3 -Wp,-D_FORTIFY_SOURCE=2"
else
    DBG="-ggdb3"
fi
#CFLAGS="-pipe $WARN $OPT $DBG -Wno-missing-field-initializers"
CFLAGS="-pipe $WARN $OPT $DBG"

if [ $ARCH = x86_64 ]; then
    ARCH_BITS=64
else
    ARCH_BITS=32
fi

# To build i915/i965 to run on multiple architectures:
#
# ARCH_CFLAGS='-m64 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64'
#              
# ARCH_CFLAGS='-m32 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64 -mfpmath=sse'
#
# To build i915/i965 to run on the build system only:
#
# ARCH_CFLAGS='-m64 -march=native'
#              
# ARCH_CFLAGS='-m32 -march=native -mfpmath=sse'

case $mode in
native)
	ARCH_CFLAGS="-m$ARCH_BITS -march=native"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
generic)
	ARCH_CFLAGS="-m$ARCH_BITS -mtune=generic -march=nocona -msahf -mcx16 \
                     --param l1-cache-line-size=64"

	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
fedora)
	debug=''
	if gcc -v 2>&1 | grep -q 'gcc version 4.[0-7]'; then
	    PROTECT='-fstack-protector --param=ssp-buffer-size=4'
	else
	    PROTECT='-fstack-protector-strong --param=ssp-buffer-size=4'
	fi

	CFLAGS="-O2 -g -pipe $WARN -Wp,-D_FORTIFY_SOURCE=2 -fexceptions $PROTECT -grecord-gcc-switches"
	if [ $ARCH = x86_64 ]; then
	    ARCH_CFLAGS="-m64 -mtune=generic"
	else
	    ARCH_CFLAGS="-m32 -march=i686 -mtune=atom -fasynchronous-unwind-tables"
	fi
	;;
*)
	echo "Invalid mode $mode"
	exit 1
	;;
esac

CFLAGS="$CFLAGS $ARCH_CFLAGS"
export CFLAGS

export BRANCH
export CXXFLAGS="$CFLAGS"
export ACLOCAL="aclocal -I $prefix/share/aclocal/"

if [ "x$base_dir" = "x" ]; then
    base_dir=$BRANCH
fi

export base_dir

for xxx in 1; do
    DIR=${base_dir}-${ARCH_BITS}

    if [ $ARCH_BITS -eq 32 ]; then
        LIB=lib
    else
        LIB=lib64
    fi
    
    if [ $do_clean -ne 0 ]; then
	echo cleaning $ARCH_BITS
	rm -rf $DIR
    fi

    # The extra "../" in the setting of src_dir is intentional.
    if [ -d ../SOURCE/$BRANCH ] ; then
	src_dir=../../SOURCE/$BRANCH
    elif [ -d ../SOURCE/Mesa-$BRANCH ] ; then
	src_dir=../../SOURCE/Mesa-$BRANCH
    else
	echo FAIL
	notify "No Mesa source $(hostname)"
	exit 1
    fi

    export GIT_DIR=$src_dir/.git

    if [ ! -d $DIR ]; then
        mkdir $DIR
	pushd $DIR > /dev/null

	lndir -silent -withrevinfo $src_dir

	# Just in case there's a configure script in the source directory.
	# This can happen after doing 'make tarballs' for example.
	if [ $do_clean -ne 0 ]; then
	    rm -f configure
	fi
	popd > /dev/null
    fi

    export PKG_CONFIG_PATH="$prefix/$LIB/pkgconfig"
    if [ ! -f $DIR/configure ]; then
        pushd $DIR > /dev/null

	if [ $gallium = yes ]; then
	    gallium_opt="--with-gallium-drivers=swrast --disable-gallium-llvm"
#	    gallium_opt="--with-gallium-drivers=i915,ilo,nouveau,r300,r600,radeonsi,freedreno,svga,swrast --enable-gallium-llvm"
	else
	    gallium_opt="--disable-gallium-llvm --disable-gallium-egl"

	    if grep -q x.with_gallium_drivers ./configure.ac; then
		gallium_opt="$gallium_opt --without-gallium-drivers"
	    else
		gallium_opt="$gallium_opt --with-gallium-drivers=swrast --disable-gallium --disable-gallium-r300"
	    fi
	fi

	COMMON_OPTS="--prefix=$prefix --libdir=$prefix/$LIB \
	    --disable-xvmc --disable-vdpau \
	    --enable-xcb \
	    --enable-dri \
	    --enable-glx \
	    --enable-egl \
	    --with-egl-platforms=x11 \
	    --with-dri-drivers=$drivers \
	    $debug $gallium_opt \
	    --disable-gallium-g3dvl \
	    --enable-glx-tls \
	    --enable-texture-float \
	    --enable-shared-glapi \
	    --enable-gles1 \
	    --enable-gles2"

	echo ./autogen.sh $COMMON_OPTS

	./autogen.sh $COMMON_OPTS
	popd > /dev/null
    fi

    echo building $ARCH_BITS
    make -C $DIR $config 2>&1

    if [ $? -ne 0 ]; then
	echo FAIL
#	notify "Mesa compilation failed for arch $i on $(hostname)"
	exit 1
    fi

    if [ -d $DIR/src/gtest ]; then
	if [ -f $GIT_DIR/BISECT_START ]; then
# -o true
	    echo "Skipping \"make check\" during bisect."
	else
	    if grep -q check $DIR/Makefile; then
		echo testing $ARCH_BITS
		make -C $DIR check 2>&1
	    fi

	    if [ $? -ne 0 ]; then
		echo FAIL
#		notify "Mesa compilation failed for arch $ARCH_BITS on $(hostname)"
		exit 1
	    fi
	fi
    fi

    if [ $do_install -ne 0 ]; then
        echo installing $ARCH_BITS
        make -C $DIR install INSTALL=/usr/bin/install 2>&1 >/dev/null
    fi
done 2>&1 | tee /tmp/mesa_alt.txt

if ! tail /tmp/mesa_alt.txt | grep -q FAIL
then
    echo PASS
#    notify "Mesa compilation succeeded on $(hostname)"
    exit 0
else
    exit 1
fi

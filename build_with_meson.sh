#!/bin/bash

function usage
{
    echo "$1 [-c] [-b branch] [-d \"driver,driver,..\"]"
}

function notify
{
#	echo $1 | festival --tts &
	if [ "x$DISPLAY" != "x" ]; then
		if which notify-send > /dev/null; then
	    		notify-send -t 600000 "$1"
		fi
	else
		echo $1
	fi
}

if [ -d /opt/idr/devel/graphics/Mesa/BUILD ]; then
    cd /opt/idr/devel/graphics/Mesa/BUILD
else
    cd ~/devel/graphics/Mesa/BUILD
fi

typeset -i do_clean=0
typeset BRANCH=master
typeset drivers
typeset prefix=''
typeset -i do_install=0

ARCH=$(uname -m)

if grep -q Atom /proc/cpuinfo ; then
    drivers=""
    gallium="i915"
else
    drivers=""
    gallium="iris"
fi

buildtype="debug"

ARCHES=''
llvm="enabled"
mode="x86-64-v2"
vulkan=''
extra_flags=''

typeset -i reconfigure=0
typeset -i quieter=0
typeset -i raytrace=1

while getopts "cb:d:D:G:iI:lm:O:p:qRr:t:v:" flag
do
    case $flag in
    c) do_clean=1;;
    b) BRANCH=$OPTARG;;
    d) drivers=$OPTARG;;
    D) base_dir=$OPTARG;;
    G) gallium="$OPTARG";;
    i) do_install=1;;
    I) install_dir="$OPTARG";;
    l) llvm="enabled";;
    m) mode=$OPTARG;;
    O) extra_cflags="$OPTARG";;
    p) prefix=$OPTARG;;
    q) quieter=1;;
    r) raytrace=$OPTARG;;
    R) reconfigure=1;;
    t) buildtype=$OPTARG;;
    v) vulkan="$OPTARG";;
    *) usage $0; exit 2;;
    esac
done

# The swrast Vulkan driver requires that the swrast Gallium driver also be
# built.  Take care of that dependency.  This does "break" some other things
# in the script that try to avoid building OpenGL stuff when no Gallium
# drivers are enabled. *shrug*
if echo "$vulkan" | grep -q swrast; then
    if echo "$gallium" | grep -q swrast; then
	:
    else
	gallium="${gallium:+${gallium},}swrast"
    fi
    llvm="enabled"
fi

if echo "$vulkan" | grep -q intel; then
    clc="-D intel-clc=enabled"


    if [ $raytrace -eq 0 ]; then
	clc="-D intel-clc=enabled -Dintel-rt=disabled"
    fi
else
    clc=""
fi

if [ $quieter -eq 0 ]; then
    WARN="-Wextra -Wunsafe-loop-optimizations -Wno-unused-parameter"
fi

if [ "x$buildtype" == "xdebug" ]; then
    OPT="-O0 -fno-omit-frame-pointer -fsanitize=shift"
    DBG="-DDEBUG"
elif [ "x$buildtype" == "xdebugoptimized" ]; then
    OPT="-fno-omit-frame-pointer"
    DBG="-DDEBUG"
fi
CFLAGS="-pipe -ggdb3 $WARN $OPT $DBG $extra_cflags"

if [ $ARCH = x86_64 ]; then
    ARCH_BITS=64
else
    ARCH_BITS=32
fi

# To build i915/i965 to run on multiple architectures:
#
# ARCH_CFLAGS='-m64 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64'
#              
# ARCH_CFLAGS='-m32 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64 -mfpmath=sse'
#
# To build i915/i965 to run on the build system only:
#
# ARCH_CFLAGS='-m64 -march=native'
#              
# ARCH_CFLAGS='-m32 -march=native -mfpmath=sse'

case $mode in
native)
	if [ "x$DISTCC_HOSTS" = "x" ]; then
	    ARCH_CFLAGS="-m$ARCH_BITS -march=native"
	else
	    ARCH_CFLAGS="-m$ARCH_BITS -march=broadwell"
	fi

	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -msse2 -mfpmath=sse -mstackrealign"
	fi
	;;
byt)
	ARCH_CFLAGS="-m$ARCH_BITS -march=silvermont"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -msse2 -mfpmath=sse -mstackrealign"
	fi
	;;
haswell|k8-sse3|amdfam10|x86-64-v2)
	ARCH_CFLAGS="-m$ARCH_BITS -march=$mode"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -msse2 -mfpmath=sse -mstackrealign"
	fi
	;;
generic)
	ARCH_CFLAGS="-m$ARCH_BITS"

	# Even the lowly Athlon 64 X2 4800+ in my collection has SSE3.
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -msse3 -mfpmath=sse -mstackrealign"
	else
	    ARCH_CFLAGS="$ARCH_CFLAGS -msse3"
	fi
	;;
fedora)
	# From /lib/rpm/redhat/macros on Fedora 38
	_general_options="-O2 -fexceptions -g -grecord-gcc-switches -pipe"
	_warning_options="-Wall -Werror=format-security"
	_preprocessor_defines="-Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS"

	__global_compiler_flags="$_general_options $_warning_options $_preprocessor_defines"

	debug='-D b_ndebug=true'

	# From /lib/rpm/redhat/rpmrc
	CFLAGS="$__global_compiler_flags -fstack-protector-strong"
	if [ $ARCH = x86_64 ]; then
	    ARCH_CFLAGS="-m64 -mtune=generic"
	else
	    ARCH_CFLAGS="-m32 -march=i686 -mtune=generic -msse2 -mfpmath=sse -mstackrealign"
	fi

	buildtype="plain"
	;;
*)
	echo "Invalid mode $mode"
	exit 2
	;;
esac

CFLAGS="$CFLAGS $ARCH_CFLAGS -Wno-missing-field-initializers"
export CFLAGS

export BRANCH

CXXFLAGS=$(echo "$CFLAGS" | sed 's/-Werror=implicit-function-declaration//;s/-Werror=missing-prototypes//;s/-Werror=incompatible-pointer-types//;s/-Werror=int-conversion//')
export CXXFLAGS

if [ "x$prefix" != "x" ]; then
    export ACLOCAL="aclocal -I $prefix/share/aclocal/"
fi

if [ "x$base_dir" = "x" ]; then
    base_dir=$BRANCH
fi

export base_dir

LOG=./mesa_build.$$.txt

build_dir=${base_dir}-${ARCH_BITS}.build

if [ "x$install_dir" == "x" ]; then
    install_dir=${base_dir}-${ARCH_BITS}
else
    install_dir=${install_dir}-${ARCH_BITS}
fi

if [ $ARCH_BITS -eq 32 ]; then
    LIB=lib
    TOOLS=""
    CROSS="--cross-file=/home/idr/devel/graphics/Mesa/scripts/meson-cross-x86-linux-gnu"
else
    LIB=lib64
    TOOLS="-D tools=drm-shim,intel"
    CROSS=""
fi

if [ -d ../SOURCE/$BRANCH ] ; then
    src_dir=../SOURCE/$BRANCH
elif [ -d ../SOURCE/Mesa-$BRANCH ] ; then
    src_dir=../SOURCE/Mesa-$BRANCH
else
    echo FAIL
    notify "No Mesa source $(hostname)"
    exit 2
fi

export GIT_DIR=$src_dir/.git

if [ "x$prefix" != "x" ]; then
    export PKG_CONFIG_PATH="$prefix/$LIB/pkgconfig:$prefix/share/pkgconfig"
fi

if [ ! -d $build_dir -o ! -f $build_dir/build.ninja ]; then
    reconfigure=0
fi

for xxx in 1; do
    if [ $do_clean -ne 0 ]; then
	echo cleaning $ARCH_BITS
	rm -rf $build_dir $install_dir
    fi

    if [ ! -d $build_dir -o ! -f $build_dir/build.ninja -o $reconfigure -ne 0 ]; then
	if [ "x$drivers" = "x" -a "x$gallium" = "x" ]; then
	    glstuff="-D gles1=disabled -D gles2=disabled -D opengl=disabled"
	else
	    glstuff="-D glx=dri -D egl=enabled \
		     -D shared-glapi=enabled -D gles1=enabled -D gles2=enabled -D opengl=true"
	fi

	COMMON_OPTS="--prefix=$PWD/$install_dir \
		-D buildtype=$buildtype \
		-D platforms=x11,wayland \
		-D llvm=$llvm \
		-D gallium-drivers=$gallium \
		-D gallium-va=disabled \
		-D vulkan-drivers=$vulkan \
		$clc
		$glstuff
		-D build-tests=true \
		$TOOLS $CROSS $debug"

	if [ "x$drivers" != "x" ]; then
	    COMMON_OPTS="$COMMON_OPTS -D dri-drivers=$drivers"
	fi

	echo CFLAGS=\"$CFLAGS\"
	echo CXXFLAGS=\"$CXXFLAGS\"
	echo meson setup $COMMON_OPTS $src_dir $build_dir
	if [ $reconfigure -eq 0 ]; then
	    if [ "x$DISTCC_HOSTS" == "x" ]; then
		meson setup $COMMON_OPTS $src_dir $build_dir
	    else
		CC='ccache distcc gcc' CXX='ccache distcc g++' meson setup $COMMON_OPTS $src_dir $build_dir
		rm -f /home/idr/.distcc/lock/backoff_*
	    fi
	else
	    meson configure $COMMON_OPTS -D c_args="$CFLAGS" $build_dir
	fi
    fi

    # $build_dir may not exit before running meson (just above here), so we
    # can't just redirect to it at the beginning.
    rm -f $build_dir/mesa_build.txt
    ln $LOG $build_dir/mesa_build.txt
    rm $LOG

    echo building $ARCH_BITS
    if [ "x$DISTCC_HOSTS" == "x" ]; then
	ninja -j$(getconf _NPROCESSORS_ONLN) -C $build_dir 2>&1
    else
	if [ "x$J" == "x" ]; then
	    echo "J not set.  Guessing 16."
	    J=16
	fi

	CC='ccache distcc gcc' CXX='ccache distcc g++' ninja -j$J -C $build_dir 2>&1
    fi

    if [ $? -ne 0 ]; then
	echo FAIL
#	notify "Mesa compilation failed for arch $i on $(hostname)"
	exit 125
    fi

    ninja -C $build_dir install

    # compiler/test_eu_compact currently fails on 32-bit
    if [ -f $GIT_DIR/BISECT_START -o $ARCH_BITS -eq 32 ]; then
# -o true
	echo "Skipping \"make check\" during bisect."
    else
	echo testing $ARCH_BITS
	ninja -C $build_dir test 2>&1

	if [ $? -ne 0 ]; then
	    echo FAIL
#	    notify "Mesa compilation failed for arch $ARCH_BITS on $(hostname)"
	    exit 1
	fi
    fi

done 2>&1 | tee $LOG

if ! tail ${build_dir}/mesa_build.txt | grep -q -E 'FAIL($|[^:])'
then
    echo PASS
#    notify "Mesa compilation succeeded on $(hostname)"
    exit 0
else
    exit 1
fi

num_warn=$(grep warning: ${build_dir}/mesa_build.txt | wc -l)
echo $num_warn warnings this build.

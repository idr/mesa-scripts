#!/bin/bash

export vblank_mode=0
export MESA_DEBUG=silent

git --no-pager log --oneline HEAD^..
step=$(git log --oneline origin/main.. | wc -l)

SHA=$(git log --oneline -1 --pretty=format:'%h' HEAD)
PREVSHA=$(git log --oneline -1 --pretty=format:'%h' HEAD^)

cd /home/idr/devel/graphics/Mesa/BUILD

if [ "x$1" = "x" ]; then
    buildtype="debugoptimized"
else
    buildtype=$1
    shift
fi

if [ "x$1" = "x" ]; then
    base="master"
else
    base=$1
    shift
fi

#drivers="iris,crocus,i915"
#drivers="iris,crocus"
drivers="iris"

date
echo Building drivers $drivers for $buildtype...
/home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -G $drivers -m native -t $buildtype -v intel -l -D $buildtype -R -I $SHA >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo Build fail
    exit 125
fi

if find $SHA-64 -name '*_dri.so' -o -name 'libvulkan_*.so' | read ; then
    size $(find $SHA-64 -name '*_dri.so' -o -name 'libvulkan_*.so' | sort)
else
    exit 255
fi
#exit 0

CMD=$(basename $0)

if [ "x$CMD" = "xrebase_shader-db.sh" ]; then
    date
    echo shader-db...
    cd /home/idr/devel/graphics/shader-db
    #mesa $SHA ./run shaders > $(printf "results-rebase-%02d.txt" $step) 2> /dev/null

    if echo "$drivers" | grep -q iris; then
	PLAT=(MTL-Meteor_Lake-iris DG2-DG2-iris TGL-Tiger_Lake-iris ICL-Ice_Lake-iris SKL-Skylake-iris BDW-Broadwell-iris)
	VK_PLAT=(MTL-Meteor_Lake-anv DG2-DG2-anv TGL-Tiger_Lake-anv ICL-Ice_Lake-anv SKL-Skylake-anv)
    else
	PLAT=()
	VK_PLAT=()
    fi

    if echo "$drivers" | grep -q crocus; then
	PLAT+=(HSW-Haswell-crocus IVB-Ivy_Bridge-crocus SNB-Sandy_Bridge-crocus ILK-Iron_Lake-crocus G4X-GM45-crocus)
    fi

    if echo "$drivers" | grep -q i915; then
	PLAT+=(LPT-i915-i915)
    fi

    #PLAT=()
    #PLAT=(MTL-Meteor_Lake-iris ICL-Ice_Lake-iris SKL-Skylake-iris)
    #PLAT=(DG2-DG2-iris TGL-Tiger_Lake-iris BDW-Broadwell-iris HSW-Haswell-crocus IVB-Ivy_Bridge-crocus SNB-Sandy_Bridge-crocus ILK-Iron_Lake-crocus G4X-GM45-crocus LPT-i915-i915)
    #PLAT=(DG2-DG2-iris TGL-Tiger_Lake-iris ICL-Ice_Lake-iris SKL-Skylake-iris BDW-Broadwell-iris)
    #PLAT=(DG2-DG2-iris TGL-Tiger_Lake-iris)
    #PLAT=(BDW-Broadwell-iris LPT-i915-i915)
    #PLAT=(SKL-Skylake BDW-Broadwell HSW-Haswell IVB-Ivy_Bridge SNB-Sandy_Bridge ILK-Iron_Lake G4X-GM45)
    #PLAT=(ICL-Ice_Lake-iris ILK-Iron_Lake G4X-GM45)
    #PLAT=(MTL-Meteor_Lake-iris DG2-DG2-iris)

    #PLAT=(MTL-Meteor_Lake-iris)
    #PLAT=(DG2-DG2-iris)
    #PLAT=(SKL-Skylake BDW-Broadwell HSW-Haswell IVB-Ivy_Bridge SNB-Sandy_Bridge)
    #PLAT=(SKL-Skylake BDW-Broadwell)
    #PLAT=(ICL-Icelake-iris)
    #PLAT=(SKL-Skylake)
    #PLAT=(HSW-Haswell)
    #PLAT=(ILK-Iron_Lake G4X-GM45)

    #VK_PLAT=(MTL-Meteor_Lake-anv ICL-Ice_Lake-anv SKL-Skylake-anv)
    #VK_PLAT=(DG2-DG2-anv TGL-Tiger_Lake-anv)
    #VK_PLAT=(MTL-Meteor_Lake-anv DG2-DG2-anv)
    #VK_PLAT=(MTL-Meteor_Lake-anv)
    #VK_PLAT=(DG2-DG2-anv)
    #VK_PLAT=(ICL-Ice_Lake-anv)

    #VK_PLAT=(polaris10-Polaris-radv gfx900-Vega-radv)

    if [ -c /dev/dri/renderD129 ]; then
	device=1
    else
	device=0
    fi

    for x in ${PLAT[*]}; do
	a=$(echo $x | cut -d- -f1)
	b=$(echo $x | cut -d- -f2)
	c=$(echo $x | cut -d- -f3)

	if [ "x$c" != "x" ]; then
	    driver="-o $c"
	else
	    driver=""
	fi

	resultname=$(printf "results-%s-%02d-%s.txt" $a $step $SHA)
	prevresultname=$(printf "results-%s-%02d-%s.txt" $a $((step - 1)) $PREVSHA)
	echo $b | sed 's/_/ /'

	plat=$(echo $a | awk '{print(tolower($0))}')

	# There's just so much error spam from i915 in shader-db.
	if [ "x$plat" = "xlpt" ] ; then
	    mesa $SHA intel_stub_gpu -p $plat ./run $driver shaders/ 2> /dev/null > $resultname
	else
	    mesa $SHA intel_stub_gpu -p $plat ./run $driver shaders/ 2>&1 > $resultname | grep -v -E '^(SKIP: shaders|ATTENTION: default value of option |Mesa warning: couldn|^WARNING: i965 does not fully support|^Instability or lower performance might)'
	fi

	if [ -f $prevresultname ]; then
	    ./report.py -c -s $prevresultname $resultname
	fi
	echo
    done

    export PATH=$PATH:/home/idr/devel/graphics/Fossilize/build/cli

    n=$(getconf _NPROCESSORS_ONLN)
    for x in ${VK_PLAT[*]}; do
	a=$(echo $x | cut -d- -f1)
	b=$(echo $x | cut -d- -f2)
	c=$(echo $x | cut -d- -f3)

	resultname=$(printf "results-%s-%02d-%s.csv" $a $step $SHA)
	prevresultname=$(printf "results-%s-%02d-%s.csv" $a $((step - 1)) $PREVSHA)
	echo $b | sed 's/_/ /'

	unset RADV_FORCE_FAMILY
	if [ "x$c" = "xanv" ]; then
            plat=$(echo $a | awk '{print(tolower($0))}')

	     mesa $SHA intel_stub_gpu -p $plat \
		./fossil_replay_parallel.sh fossils/ $resultname --num-threads $n 2>&1 | grep -v '^Fossilize INFO:'
	     if [ $? -ne 0 ]; then
		 exit 125
	     fi

	     if find . -name "${resultname}.part_*" -print -quit | grep -q part_ ; then
		 exit 125
	     fi

	elif [ "x$c" = "xradv" ]; then
	    RADV_FORCE_FAMILY=$a mesa $SHA \
		./fossil_replay.sh fossils/ $resultname --num-threads $n 2>&1 | grep -v '^Fossilize INFO:'
	else
	    echo "Invalid Vulkan moniker $c"
	fi


	if [ -f $prevresultname ]; then
	    ./report-fossil.py --hide-table $prevresultname $resultname
	fi
	echo
    done

    exit 0
fi

if [ "x$CMD" = "xrebase_massif.sh" ]; then
    cd /home/idr/devel/graphics/shader-db

    for f in shaders/closed/steam/dirt-showdown/676.shader_test \
	     shaders/closed/steam/deus-ex-mankind-divided/2890.shader_test \
	     shaders/closed/steam/deus-ex-mankind-divided/148.shader_test \
	     shaders/closed/gfxbench5/aztec-ruins/high/11.shader_test \
	     shaders/dolphin/ubershaders/210.shader_test \
	     /home/idr/devel/graphics/piglit/tests/shaders/mean-soft-fp64-using-uint64.shader_test; do
	suffix=$(echo $f | sed 's/.shader_test$//;s|/|_|g')
	for x in $(seq 3); do
	    while true; do
		typeset -i l
		l=$(cat /proc/loadavg | cut -f1 -d.)
		if [ $l -lt 4 ]; then
		    break
		fi

		sleep 5
	    done

	    mesa $SHA valgrind --tool=massif --trace-children=yes --massif-out-file=massif.$SHA-$suffix.$x \
		 ./intel_run -p SKL -j 1 $f &
	done
    done
    wait
fi
